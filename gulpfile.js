// http://ilikekillnerds.com/2014/07/how-to-basic-tasks-in-gulp-js/


// Include gulp and plugins
var gulp         = require('gulp'),
    browserSync  = require('browser-sync').create(),
    sass         = require('gulp-sass'),
    rename       = require("gulp-rename"),
    uglify       = require('gulp-uglify'),
    uncss        = require('gulp-uncss'),
    notify       = require("gulp-notify"),
    plumber      = require('gulp-plumber'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps')    

    // Makes getting around easier on varying degrees
    THEME        = 'wp-content/themes/custom-timber-theme'



// Static Server + watching scss/html files
gulp.task('serve', function() {
    browserSync.init({
        baseDir: "./"
    });
    gulp.watch(THEME + "/assets/stylesheets/application.css", ['sass']);
    gulp.watch(THEME + "/*.php").on('change', browserSync.reload);
    gulp.watch(THEME + "/**/*.php").on('change', browserSync.reload);
    gulp.watch(THEME + "/templates/*.twig").on('change', browserSync.reload);
    gulp.watch(THEME + "/**/*.twig").on('change', browserSync.reload);
    gulp.watch(THEME + "/assets/javascript/application.min.js").on('change', browserSync.reload);
});




gulp.task('sass', function() {
  return gulp.src(THEME + '/assets/stylesheets/*.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({
        style: 'compressed',
        errLogToConsole: false,
    }))
    .on('error', function(err) {
        notify().write(err);
        this.emit('end');
    })    
    .pipe(plumber.stop())
    .pipe(sourcemaps.write())
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))    
    .pipe(gulp.dest(THEME + '/assets/stylesheets'))
    .pipe(notify({ message: 'Styles task complete' }))
    .pipe(browserSync.stream());
}); 




// Uglify and Rename our Application JS
// Still Need to set this up for our vender folder as well

gulp.task('compress', function() {
  return gulp.src(THEME + '/assets/javascript/application.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(rename(function (path) {
      path.dirname += "/javascript";
      path.basename += ".min";
      path.extname = ".js"
    }))
    .pipe(gulp.dest(THEME + '/assets/'))
    .pipe(plumber.stop())
    .pipe(notify({ message: 'Javascript task complete' }));
});




// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(THEME + '/assets/**/*.scss', ['sass']);
    gulp.watch(THEME + '/assets/javascript/application.js', ['compress']);
});



// Default Task
gulp.task('default', ['sass', 'compress', 'watch', 'serve']);
