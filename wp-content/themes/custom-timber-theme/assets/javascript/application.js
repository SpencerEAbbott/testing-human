
jQuery(document).ready(function($) {

  // simple tabs for Studio Settings
  $(".menu-item--list-item a").click(function(event) {
      event.preventDefault();      
      $(this).parent().addClass("current");
      $(this).parent().siblings().removeClass("current");
      var tab = $(this).attr("href");
      $(".menu-item--content").not(tab).css("display", "none");
      $(tab).fadeIn();
  });



  // Toggle showing element details for each studio class available
  $(".location .header--location-name").click(function(event) {
      $(this).parent().toggleClass("selected");
  });




// dropdown custom UI
// source: http://tympanus.net/codrops/2012/10/04/custom-drop-down-list-styling/
// Demo 3
  function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;
    this.initEvents();
    this.hiddenInput = this.dd.find('input.state-hidden-input');
  }
  DropDown.prototype = {
    initEvents : function() {
      var obj = this;
      // add proper classes to for when active and once state has been seleced
      obj.dd.on('click', function(event){
        $(this).toggleClass('active');
        $(this).addClass('updated');

        return false;
      });
      // updates placeholder
      obj.opts.on('click',function(){
        var opt = $(this);
        obj.val = opt.text();
        obj.index = opt.index();
        obj.placeholder.text(obj.val);  
        // adds value to hiden input of form
        obj.hiddenInput.attr('value', obj.val); 
        $('.studio-container').addClass('changes-made');
      });
    },
    getValue : function() {
      return this.val;
    },
    getIndex : function() {
      return this.index;
    }
  }

  // activates dropdown on click
  $(function() {
    // needs refactoring to loop thorugh all new instances of .custom-dropdown and initialize based on id
    // manual works for MVP
    var dd = new DropDown( $('#state-dd-1') );
    var dd = new DropDown( $('#state-dd-2') );
    var dd = new DropDown( $('#location-dd') );
    

    $(document).click(function() {
      // all dropdowns
      $('.custom-dropdown').removeClass('active');      
    });
  });






  // Updates ability to save form once a change has been made
  // watches for change at all times, vs .change which requires an exit from input
  // FORMS CURRENTLY HAVE NO VALIDATION, WILL ACCEPT ANYTHING
  $('.location-form input, .location-form select').each(function() {
    var elem = $(this);

    // Save current value of element
    elem.data('oldVal', elem.val());

    // Look for changes in the value
    elem.bind("propertychange change click keyup input paste value", function(event){
      // If value has changed...
      if (elem.data('oldVal') != elem.val()) {
       // Updated stored value
       elem.data('oldVal', elem.val());
       // Do action
       $(".studio-container").addClass("changes-made");
     }
    });     
 });


  // store inital state of form data
  $(function(){
      $('form').find(':input').each(function(i, elem) {
           var input = $(elem);
           input.data('initialState', input.val());
      });
  });


  // restores data to initial state when initialized
  function restore() {
      $('form').find(':input').each(function(i, elem) {
           var input = $(elem);
           event.preventDefault();    
           input.val(input.data('initialState'));
      });
  }


  // initialize restore function when Cancel is clicked. 
  $('.button--cancel').click(restore);




}); // End jQuery hack from top




