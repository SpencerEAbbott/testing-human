# Welcome!

Thanks for taking the time to look at my work for your code test. 

## Enviroment

I choose to use a custom wordpress template with the Timber plugin, enabling the use of Twig templating language (shared by CraftCMS). I choose this because it allowed me to modularize sections in ways I wanted to in a fairly time effective manner. This is becacuse I had a bit of a boilerplate for this set up already. Also, my server was already set up to host wordpress sites and do multiple domains. 

So while this aspect of the build may have seem a bit convuluted, it was fairly simple considering the work I had already done. 

## Tools

I chose to use Gulp, SASS, Twig, and JQuery, and SVG Sprites for this build. These are the tools that I am currently most familiar with

## Where's the code I care about?

Great question!

####Twig:
wp-content -> themes -> custom-timber-theme -> templates

####Styles and Logic:
wp-content -> themes -> custom-timber-theme -> assets